# Multi Factor Authentication Formal Verification

## Intro

We provide in this package scripts and models to check
the security of different authentication protocols.
The proverif .pv are generated automatically, from the files
 without extension within login-protocol-trust. 
Each file contain one protocol, and describe different threat
 models usinig the cpp preprocessor.

We use includes to have a bit of factoring. The different properties
we check can be found in login-protocol-trust/queries.

The corresponding paper can be found [here](https://easychair.org/publications/preprint/m89p).

## Requirements

The files were executed successfully using proverif 1.97pl3, and 
gcc version 5.4.0 20160609 (Ubuntu 5.4.0-6ubuntu1~16.04.5),
and python 3.5.2

Some tests were also made to make the preprocessing compatible
with clang and mac os.

The python script is using multiprocessing, and may use as much
 core as possible for some time.

Proverif can be found here :
http://prosecco.gforge.inria.fr/personal/bblanche/proverif/

If you have opam completely set up, one simply has to do :
`opam install proverif`

## Bash script

To instantiate each scenario, we provide check.sh
It basically run gcc and proverif on the specified file, with 
the defined scenarios passed in parameters.

`./check.sh login-protocols-trust/g2V PH LOGIN` will run g2V under phishing.

## Main script

Our main tools is based on the previous two scripts, and is auto_paper.py.
It computes the list of all possible scenarios, runs proverif for all given
protocols and scenarios, and finally produces a tex array with all the 
pertinent results.

`auto_paper.py -h` provides the different arguments.

`auto_paper.py` without any argument should produce the tex file provided in the
 results folder, which run all possible scenarios on all the protocols.

To generate a tex file containing the results for one protocol, one may type
`auto_paper.py -p g2V`, which will generate `g2V.tex`.

To generate a diff between two protocols, one may run
`auto_paper.py -p g2DTD TB-g2DTD -c "g2DTD TB-g2DTD"`

It is possible to specify only some scenarios to check using the -s parameter,
we refer the reader to the script to have the list of possible scenarios.

## Cleaning up

Calling auto_paper may generate up to several thousands of .pv files.
We invite the user to run clean.sh to remove all unwanted files after its execution.

## Save/Load data

To produce many different tables, it might be necessary to recompute several time the same proverif result. To avoid this, we have a basic save/load data.
It requires the numpy python package, for installable through pip : `pip install numpy`.
One can save all the datas in some file through the command:
`./auto_paper.py -fs db`
After some computation time, this will produce a file `db.npy`, which contains all the analysis of the protocol.
Then, to directly produce the desired table by using the data in the file, on can for instance launch:
`./auto_paper.py -p g2V g2ST g2STFPR -c "g2ST g2STFPR" -fl db`
This should in a few seconds, and without relaunching proverif, provide the desired pdf file.

## Paper figures

Bellow are the command used to generate the figures of the CSF18 paper.
Some outputs were edited by hand, see the tex files in the results folder for comments.

>table 1: ./auto_paper.py -p g2V g2ST g2STFPR -c "g2ST g2STFPR"
>
>2:./auto_paper.py -p g2V g2ST g2STFPR g2DTFPR -c "g2STFPR g2DTFPR" -d g2DTFPR -co
>
>3: ./auto_paper.py -p g2V g2VFPR g2VD g2STFPR g2STD g2DTFPR g2DTD -c "g2V g2VFPR" "g2VFPR g2VD" "g2STFPR g2STD" "g2DTFPR g2DTD" -d g2VFPR g2VD g2STD g2DTD -co
>
>4: ./auto_paper.py -p g2VD g2DTD -c "g2VD g2DTD" "g2DTD g2VD" -co
>
>5:  ./auto_paper.py -p U2F
>
>6: ./auto_paper.py -p U2F TB-U2F g2DTD TB-g2DTD -c "U2F TB-U2F" "g2DTD TB-g2DTD" -co
>
>7: ./auto_paper.py -p g2DTD g2DTDE -c "g2DTD g2DTDE" -co
>
>8: ./auto_paper.py -p U2F g2DTDE -c "U2F g2DTDE"  "g2DTDE U2F" -co

## Unlinkability

We also provide an analysis of the unlinkability of the U2F Yubikey token in the folder unlinkability.
The two .pv files may be run with proverif directly.
