#!/bin/bash
# Run the C preprocessor on the input file, produces a .pv file and runs proverif on it

res=""
name=""
for def in ${@:2}
do
    res+=" --define-macro=$def=$def"
name+="$def"
done
if [ "$name" = "" ]; then
  file="$1.pv"
else	
  file="$1-$name.pv" 
fi
eval "(
echo  \"(* !!! WARNING !!! *)\";
echo \"(* File generated from with ./check.sh $@*)\";
echo \"(* Read the README for more informations *)\";
echo \"(* ------------------------------------- *)\"; 
cpp -P -E -w $res $1;
) > $file; 
proverif $file"
