(*

 ------------ Google 2 step -------------
 ----    Single Tap Display Version  ----


Processes:
 P = the trusted platform
 AP = the untrusted platform
 S = the webserver
 D = the users device (its phone)
 H = minimal modelisation of the human
 TLS_manager = modelling of TLS behaviors

Protocol with second factor:
 H->P: (server urls, login, trust, password)
 P->S: (login, password, trust, TLS session id)
 S->D: (token, fingerprint, trust)
 D->H: (fingerprint,trust, ok ?)
 H: compare the fingerprint with its own and the intended trust
 H->D: (yes !)
 D->S: (token)

If trust is set to true, the server will deliver a cookie
to the client and allow login without the second factor
for this computer.
      -----------------------------------------------
All the scenarios are expressed in this file using c preprocessor syntax,
see the Readme for how to use proverif on it.
      -----------------------------------------------

*)

#include "headers"

#include "queries"

(* Process *)

let P=           (* the trusted platform *)
		#include "cookie_login"
	|
	
		(
		in(usb_kb,(ids:id,idh:id,pwd:pass));
		#if defined(P_USBI_RO) && !defined(P_USBI_RW)
    	out(a,(ids,idh,pwd));          (* in case of keylogger or malware, keyboard inputs are public *)
		#endif
		in(TLS(idH,ids),(sess_id:session,msgc:tag));
		if msgc=msg5 then
			(
			event Initiate_t(idH,idh,sess_id);
			in(usb_kb2,trust:tr);
			if trust = trusted then	
				(
				#if defined(P_TLS_RO) && !defined(P_TLS_RW)
				out(a,(idh,pwd,sess_id,trust));
				#endif
				out(TLS(idH,ids),(idh,pwd,sess_id,trust,msg2))
				|                                (* parallel composition for asynchrone communications *)
					(
					if trust = trusted then
						(
						in(TLS(idH,ids),(cookie2:bitstring,sess_id3:session,msg:tag));
						#if defined(P_TLS_RO) && !defined(P_TLS_RW)
						out(a,(cookie2,sess_id3));
						#endif	
						if msg=msg4 && sess_id3 = sess_id then
							out(memory,(cookie2,ids))
						)
					)
				)
			)
		)		
	.
 
let AP=           (* the untrusted platform *)
	in(a_usb_kb,(ids:id,idh:id,pwd:pass));
	#if defined(AP_USBI_RO) && !defined(AP_USBI_RW)
    out(a,(ids,idh,pwd));          (* in case of keylogger or malware, keyboard inputs are public *)
	#endif	
		(
		in(TLS(idA,ids),(sess_id:session,msgc:tag));
		if msgc = msg5 then
			(
			event Initiate_u(idA,idh,sess_id);
			in(a_usb_kb2,trust:tr);
				(
				#if defined(AP_TLS_RO) && !defined(AP_TLS_RW)
				out(a,(idh,pwd,sess_id,trust));
				#endif
				out(TLS(idA,ids),(idh,pwd,sess_id,trust,msg2))
				)
			)
	).
    
let S(p:pass) =                    (* the server *)
	(
	in(a,client:id);                                                            (* the server is willing to talk to anybody *)
	new r:bitstring;
	out(TLS(client,idS),(sid(r),msg5))
	|
		(
		in(TLS(client,idS),(idh:id,pwd:pass,sess_id:session,trust:tr,msg:tag)); (* but it only does so over TLS *)	
		if pwd = p && idh = idH && msg=msg2 && sess_id=sid(r) then
			(
			new token:bitstring;
			out(d_in,(fpr(client),trust,token));
			in(d_out2,token2:bitstring);
			if token2=token  then
				if trust=trusted then
					(
					event Login_trust(client,idh,sess_id); 
					new rnd:bitstring;
					out(TLS(client,idS),(cookie(rnd,client),sess_id,msg4));
					out(memory_s,cookie(rnd,client))
					)
				else
					event Login(client,idh,sess_id) 
			)
		)			
	)
	|

		#include "cookie_server"

.

let D = 
	in(d_in,(fp:fingerprint,trust:tr,token:bitstring));
    #if defined(D_I_RO) && !defined(D_I_RW)
       out(a,(fp,trust,token));
	#endif

    #if defined(D_O_RO) && !defined(D_O_RW)
       out(a,(fp,trust));
	#endif
	out(d_out,(fp,trust,token))
	|
		(
		in(d_in2,token2:bitstring);
		if token=token2 then
		    #if defined(D_O_RO) && !defined(D_O_RW)
    	    out(a,(token));
			#endif
			out(d_out2,token)
		). (* the user phone *)

let H(p:pass) =      (* modelling minimal human capabilities *)
    in(a,(idc:id,ids:id));                (* the attacker may try to send the human a url *)
	(
		if idc=idH then
		(
			(
		    if ids= idS then             (* depending on the phishing, the human will check the identity of the server, i.e its url *)
				(
				out(usb_kb,(ids,idH,p))          (* then, the human starts a login session on its computer *)
				|
					(
					out(usb_kb2,(trusted))
					
					|
						(
						event Initiate_trust(idH);
						in(d_out,(fp:fingerprint,trust:tr,token:bitstring));
						#ifndef NC
						if fp=fpr(idH) && trust = trusted then
						#endif
							out(d_in2,(token))
						)
					)
				)
			)
			|
			(
		    #ifndef PH
		    if ids= idS then             (* depending on the phishing, the human will check the identity of the server, i.e its url *)
			#endif
				(
                in(usb_kb00,ready:bitstring);
				event Initiate_trustc(idH);
				out(usb_kb0,(ids,idH,p,ready))   
				)
			)
		)
		else if idc=idA then	
		    #ifndef PH
		    if ids= idS then             (* depending on the phishing, the human will check the identity of the server, i.e its url *)
			#endif
			(
			out(a_usb_kb,(ids,idH,p))          (* then, the human starts a login session on its computer *)
			|
				(
				out(a_usb_kb2,(untrusted))
				
	   			|
					(
					event Initiate_untrust(idA);
					in(d_out,(fp:fingerprint,trust:tr,token:bitstring));
					#ifndef NC
					if fp=fpr(idA) && trust=untrusted then
					#endif
    			  		out(d_in2,(token))
					)
			    )
			)
	)
.

let TLS_manager =   (* gives access to the attacker to all compromised TLS session *)
   (
     (in(a,ida:id);
     if not(ida=idH) && not(ida=idA) then    (* the attacker can communicate over any session where the client id is compromised *)
       out(a,TLS(ida,idS)))
   |
     (in(a,(idc:id,idb:id));
     if not(idb=idS) then    (* the attacker can communicate over any session where the server id is compromised *)
      out(a,TLS(idc,idb)))
   ).

process new pwd : pass; 

#include "reveals"

#if defined(SANITY) || defined(NOINJ)
(
  AP |
  P |
  S(pwd) |
  D |
  !H(pwd) |
  TLS_manager
)
#else
(
  !AP |
  !P |
  !S(pwd) |
  !D |
  !H(pwd) |
  !TLS_manager
)
#endif
