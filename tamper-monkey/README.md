# Hiding 'I trust this computer' checkbox

## Intro

Using an extension which allows the browser to insert javascript inside pages, 
we can hide to the user the 'I trust this computer' checkbox. As it is by default checked, this may lead to unwanted behaviours. 

## TamperMonkey

We used tamper monkey v4.8.5847 and Firefox Developer edition.
One can install a script by installing and enabling the extension
and then choosing create new script. The script can then be edited,
and after having been saved, one can edit the settings of the script 
to specify one which pages he should be enabled.

In our case, we add `https://accounts.google.com/*` to the user 
includes.

## Scripts

The script can be very basic, we need to get the div element
containing the checkbox, and add a css property.
If `elt` contains the desired element, it can be done with 
`elt.setAttribute("style","visibility:hidden;");`

A small difficulty lies in the fact the Google Authentication
page appears to be generated in a procedural way. In our experiences,
the div containing the checkbox had the class name `T8qVIf`, which leads
to a simple script, shown in `ex_class.js`

If the div class name changes, the script become inefficient. A more
 resilient way to get the desired element might be using its xpath,
as shown in `ex_xpath.js`.

Both scripts might be adjusted according to the evolution of the page,
 by using the `inspect element` option of firefox, and navigating through
the element of the page. The xpath of an element can be obtained with 
a right-click, and `copy -> xpath`.
