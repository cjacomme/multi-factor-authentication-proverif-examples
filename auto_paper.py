#!/usr/bin/env python3

"""
This script allows to run proverif on a list of protocols specified 
in proverif, with some c preprocessor to specify different scenarios.

The results are computed using parallelization, and are then post 
processed for a nice display, with finally a rendering using a tex
template.

"""

import os
import sys
import signal
import subprocess
import argparse
import smtplib
import json
from email.header import Header
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from multiprocessing import Pool
import multiprocessing.pool
from functools import partial

# Classes for multiprocessing
class NoDaemonProcess(multiprocessing.Process):
    # make 'daemon' attribute always return False
    def _get_daemon(self):
        return False
    def _set_daemon(self, value):
        pass
    daemon = property(_get_daemon, _set_daemon)

# We sub-class multiprocessing.pool.Pool instead of multiprocessing.Pool
# because the latter is only a wrapper function, not a proper class.
class Pool(multiprocessing.pool.Pool):
    Process = NoDaemonProcess


# We define our different base scenarios, and combine them
network_scenarios=["", "PH", "FS", "NC", "FS NC", "PH NC", "PH FS", "PH FS NC"]
computer_scenarios_usb = ["", "P_USBI_RO", "P_USBI_RO P_USBI_RW", "P_USBI_RO P_USBO_RO", "P_USBI_RO P_USBO_RO P_USBI_RW P_USBO_RW"]
computer_scenarios_display = ["", "P_DI_RO", "P_DI_RO P_DI_RW"]
computer_scenarios_tls = ["", "P_TLS_RO", "P_TLS_RO P_TLS_RW"]
computer_scenarios_memory = ["", "P_M_RO", "P_M_RO P_M_RW"]
cn_scenarios = []
for i in network_scenarios:
    aux=[]
    for j in computer_scenarios_display:
        for k in computer_scenarios_tls:
            for l in computer_scenarios_usb:
                for m in computer_scenarios_memory:
                    scen = ' '.join(filter(None,(i,l,j,k,m)))
                    if scen:
                        aux.append(scen)
    cn_scenarios.append(aux)
acomputer_scenarios_usb = ["", "AP_USBI_RO", "AP_USBI_RO AP_USBI_RW", "AP_USBI_RO AP_USBO_RO", "AP_USBI_RO AP_USBO_RO AP_USBI_RW AP_USBO_RW"]
acomputer_scenarios_display = ["", "AP_DI_RO", "AP_DI_RO AP_DI_RW"]
acomputer_scenarios_tls = ["", "AP_TLS_RO", "AP_TLS_RO AP_TLS_RW"]
acomputer_scenarios_memory = ["", "AP_M_RO", "AP_M_RO AP_M_RW"]
acn_scenarios = []

for l in network_scenarios:
    aux = []
    for j in acomputer_scenarios_display:
        for k in acomputer_scenarios_tls:
            for i in acomputer_scenarios_usb:    
                for m in acomputer_scenarios_memory:
                    scen = ' '.join(filter(None,(l,i,j,k,m)))
                    if scen:
                        aux.append(scen)
    acn_scenarios.append(aux)




device_scenarios = [['D_I_RO', 'D_I_RO D_I_RW', 'D_I_RO D_O_RO', 'D_I_RO D_O_RO D_I_RW D_O_RW'],]

# the default timeout for a proverif execution
TIMEOUT = 3

# We define a pretty printer for the scenarios, with tex templates
scen_pprinter = {
    'D_I_RO' : "\mali{dev}{\\ro}",
    'D_I_RO D_I_RW' : "\mali{dev}{\\rw}",
    'D_I_RO D_O_RO': "\maliom{dev}{\\ro}",
    'D_I_RO D_O_RO D_I_RW D_O_RW' : "\maliom{dev}",
    "P_USBI_RO" : "\mali{t-usb}{\\ro}",
    "P_USBI_RO P_USBI_RW" : "\mali{t-usb}{\\rw}",
    "P_M_RO" : "\mali{t-hdd}{\\ro}",
    "P_M_RO P_M_RW" : "\mali{t-hdd}{\\rw}",
    "P_USBI_RO P_USBO_RO" : "\maliom{t-usb}{\\ro}", 
    "P_USBI_RO P_USBO_RO P_USBI_RW P_USBO_RW" : "\maliom{t-usb}{\\rw}",
    "P_DI_RO" : "\maliom{t-dis}{\\ro}",
    "P_DI_RO P_DI_RW" : "\maliom{t-dis}{\\rw}",
    "P_TLS_RO" : "\maliom{t-tls}{\\ro}",
    "P_TLS_RO P_TLS_RW" : "\maliom{t-tls}{\\rw}",
    "AP_M_RO" : "\mali{u-hdd}{\\ro}",
    "AP_M_RO AP_M_RW" : "\mali{u-hdd}{\\rw}",
    "AP_USBI_RO" : "\mali{u-usb}{\\ro}",
    "AP_USBI_RO AP_USBI_RW" : "\mali{u-usb}{\\rw}",
    "AP_USBI_RO AP_USBO_RO" : "\maliom{u-usb}{\\ro}", 
    "AP_USBI_RO AP_USBO_RO AP_USBI_RW AP_USBO_RW" : "\maliom{u-usb}{\\rw}",
    "AP_DI_RO" : "\maliom{u-dis}{\\ro}",
    "AP_DI_RO AP_DI_RW" : "\maliom{u-dis}{\\rw}",
    "AP_TLS_RO" : "\maliom{u-tls}{\\ro}",
    "AP_TLS_RO AP_TLS_RW" : "\maliom{u-tls}{\\rw}",

    }

list_scen_pprinter = list(scen_pprinter.keys())
list_scen_pprinter.sort(key=lambda item: len(item))
list_scen_pprinter.reverse()

def gen_tex(dprots):
    """Generates the tex array for the given list of protocols"""

    tex_template = u"""\documentclass[compsoc, conference, letterpaper, 10pt, times, table, svgnames]{article}

\\usepackage{xcolor}
\\usepackage{pifont}
\\usepackage{multicol}
\\usepackage[paperwidth=20in, paperheight=40in]{geometry}
\\newcommand{\cmark}{\\textcolor{Lime}{\ding{51}}}
\\newcommand{\cannot}{?}
\\newcommand{\\bigcmark}{\\textcolor{Lime}{\ding{52}}}
\\newcommand{\\bluecmark}{\\textcolor{Blue}{\ding{51}}}
\\newcommand{\\bigbluecmark}{\\textcolor{Blue}{\ding{52}}}
\\newcommand{\greycmark}{\\textcolor{Grey}{\ding{51}}}
\\newcommand{\\biggreycmark}{\\textcolor{Grey}{\ding{52}}}
\\newcommand{\qmark}{\\textcolor{Grey}{\ding{51}}}
\\newcommand{\\xmark}{\\textcolor{Red}{\ding{55}}}
\\newcommand{\\bigxmark}{\\textcolor{Red}{\ding{54}}}
\\newcommand{\greyxmark}{\\textcolor{Grey}{\ding{55}}}
\\newcommand{\\biggreyxmark}{\\textcolor{Grey}{\ding{54}}}

\\newcommand{\mal}{\mathcal{M}}
\\newcommand{\\ro}{\mathcal{RO}}
\\newcommand{\\rw}{\mathcal{RW}}
\\newcommand{\chan}[1]{#1}
\\newcommand{\mali}[2]{$\mal^{\chan{#1}}_{in:#2}$}
\\newcommand{\malo}[2]{$\mal^{\chan{#1}}_{out:#2}$}
\\newcommand{\malio}[3]{$\mal^{\chan{#1}}_{in:#2,out:#3}$}
\\newcommand{\maliom}[2]{$\mal^{\chan{#1}}_{#2}$}

\\begin{document}
\\begin{figure}
\\vspace{-2.5cm}
\\begin{itemize}
\item green tick = injectivity proven,  red cross = attack found, grey cross = cannot prove, \_ = scenario not pertinent
\item First mark : No unwanted login with 2 Factor and 'I trust' unchecked
\item Second mark : No unwanted login with 2 Factor and 'I trust' checked
\item Third mark : No unwanted login through cookie
\end{itemize}
\\rowcolors{1}{LightSteelBlue!60}{}
\\begin{tabular}{p{0.25cm}p{0.25cm}p{0.3cm}cc""" + "".join([ "c" for p in dprots]) + """}
"""

    end_tex_template="""\end{tabular}
\\begin{footnotesize}
Protocols
\\vspace{-.4cm}
\\begin{multicols}{2}
\\begin{itemize}
\item g2V - Google 2 Step with Verification code
\item g2VL - G2 V with code Linked to the TLS session
\item g2VLD - G2 V with code Linked to the TLS session and display
\item g2ST - Google 2 Step Single Tap
\item g2STD - G2 ST with Fingerprint display
\item g2STRD - G2 STD with a Random to compare
\item FIDO - FIDO Yubikey protocol
\item g2DTDE - g2DTD extension
\end{itemize}
\end{multicols}
\\vspace{-.4cm}
Scenarios:
\\vspace{-.4cm}
\\begin{multicols}{2}
\\begin{itemize}
\item NC - No Compare, the human does not compare values
\item FS - Fingerprint spoof, the attacker can copy the user IP address
\item WPH - The user might be victim of phishing only on trusted everyday connections
\item SPH - The user might be victim of phishing even when doing an untrusted connection
\item D\_I\_RO/D\_O\_RW/... - The phone inputs and inputs are controlled in Read Only or Read Write
\item P\_... - Attacker control over the user PC, where the user wants to stay logged in.
\item AP\_... - Attacker control over some untrusted device, where the user only want to login once.
\item TLS\_RO\_RW... - The attacker controls the TLS connections, inputs or outputs
\item USBI\_RO/USBO\_RW - Attacker control of the USB devices, inputs or outputs

\end{itemize}
\end{multicols}
\end{footnotesize}


\end{figure}
\end{document}
"""

    tex_template += """\multicolumn{4}{c}{Threat Scenarios} """
    # we compute the set of pertinent scenarios and display the protocols
    scens = set([])
    for prot in dprots:
        tex_template += """ & %s """ % prot
        scens = scens | set(results[prot].keys())
    tex_template += """\\\\
"""
    scens = list(scens)
    scens.sort(key=lambda item: (item != '', len(item)>=5, 'AP' in item, not('PH' in item), 'FS' in item, len(item)))
    print(scens)
    for scen in scens:
        pp_scen = scen
        for rw in (list_scen_pprinter):
            pp_scen = pp_scen.replace(rw,scen_pprinter[rw])
        pp_scen =  pp_scen.replace("_","\_").split(" ")
        print(pp_scen)
        if 'PH' in pp_scen:
            tex_template += """ PH & """
            pp_scen.remove("PH")
        else:  
            tex_template += """& """
        if 'FS' in pp_scen:
            tex_template += """ FS & """
            pp_scen.remove("FS")
        else:  
            tex_template += """& """
        if 'NC' in pp_scen:
            tex_template += """ NC & """
            pp_scen.remove("NC")
        else:  
            tex_template += """& """
        print(pp_scen)
        tex_template += " ".join(pp_scen)


        for prot in dprots:
            result = results[prot][scen]
            tex_template += """& """
            if result == "big_true_implied":
                tex_template += """\\biggreycmark """
            elif result == "big_true":
                tex_template += """\\bigcmark """
            elif result == "big_true_diff":
                tex_template += """\\bigbluecmark """
            elif result == "big_false_implied":
                tex_template += """\\biggreyxmark """
            elif result == "big_false":
                tex_template += """\\bigxmark """
            else:
                for res in result:
                    if "true_implied" in res:
                        tex_template += """\greycmark """
                    elif "true_diff" in res:
                        tex_template += """\\bluecmark """
                    elif "false_implied" in res:
                        tex_template += """\greyxmark """
                    elif "true" in res:
                        tex_template += """\cmark """
                    elif "false" in res:
                        tex_template += """\\xmark """
                    elif "cannot" in res:
                        tex_template += """\cannot """
                    elif "failure" in res:
                        tex_template += """- """
                    elif "noinjvalid" in res:
                        tex_template += """\qmark """
        tex_template += """\\\\
"""
    tex_template += end_tex_template
    filename = "-".join(dprots)+".tex"
    with open(filename, 'w') as res_file:
        res_file.write(tex_template)
    subprocess.call(["pdflatex", filename],stdout=subprocess.PIPE)

# utility functions to merge dictionnaries
def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z

# base function which calls the subscript computing the results
def call_check(prot,prop,scen):
    cmd = "./check.sh %s %s %s" % ("login-protocols-trust/"+prot,prop,scen)
    print(cmd)
    process = subprocess.Popen(cmd.split(),cwd=os.path.dirname(os.path.realpath(__file__)),stderr=subprocess.STDOUT,stdout=subprocess.PIPE, preexec_fn=os.setsid)
    try:
        output, errors = process.communicate(timeout=TIMEOUT)
        if "Error" in str(output) or "error" in str(output):
            return ["failure"]

        proof_results = [line for line in str(output).split('\\n') if "RESULT" in line]
        ret = []
        for res in proof_results:
            if not "even " in res:
                if "true" in res:
                    ret.append("true")
                elif "false" in res:
                    ret.append("false")
                elif "cannot" in res:
                    raise ValueError
        return ret
    except subprocess.TimeoutExpired:
        os.killpg(os.getpgid(process.pid), signal.SIGTERM) 
        cmd = "./check.sh %s %s NOINJ %s" % ("login-protocols-trust/"+prot,prop,scen)
        print(cmd)
        try:
            process = subprocess.Popen(cmd.split(),cwd=os.path.dirname(os.path.realpath(__file__)),stderr=subprocess.STDOUT,stdout=subprocess.PIPE,preexec_fn=os.setsid)
            output, errors = process.communicate(timeout=TIMEOUT)
            proof_results = [line for line in str(output).split('\\n') if "RESULT" in line]
            ret = []
            for res in proof_results:
                if not "even " in res:
                    if "true" in res:
                        ret.append("noinjvalid")
                    elif "false" in res:
                        ret.append("false")
                    elif "cannot" in res:
                        ret.append("cannot")
            return ret

        except subprocess.TimeoutExpired:
            os.killpg(os.getpgid(process.pid), signal.SIGTERM) 
            ret = ['failure']
            return ret
    except  ValueError:
        cmd = "./check.sh %s %s NOINJ %s" % ("login-protocols-trust/"+prot,prop,scen)
        print(cmd)
        try:
            process = subprocess.Popen(cmd.split(),cwd=os.path.dirname(os.path.realpath(__file__)),stderr=subprocess.STDOUT,stdout=subprocess.PIPE,preexec_fn=os.setsid)
            output, errors = process.communicate(timeout=TIMEOUT)
            proof_results = [line for line in str(output).split('\\n') if "RESULT" in line]
            ret = []
            for res in proof_results:
                if not "even " in res:
                    if "true" in res:
                        ret.append("noinjvalid")
                    elif "false" in res:
                        ret.append("false")
                    elif "cannot" in res:
                        ret.append("cannot")
            return ret

        except subprocess.TimeoutExpired:
            os.killpg(os.getpgid(process.pid), signal.SIGTERM) 
            ret = ['failure']
            return ret

# function which checks if the protocols are running
def check_sanity(prot):
    proof_results = call_check(prot, "SANITY", "")
    print(proof_results)
    for res in proof_results:
        if "true" in res or "failure" in res:
            raise "Not running protocols"

def greater(scen1,scen2):
    return scen1 != "" and all( s in scen2.split(' ') for s in scen1.split())

def greater_empty(scen1,scen2):
    return all( s in scen2.split(' ') for s in scen1.split())

def load_result_prot_scenl(scens,prot):
     prot_res = {}
     for scen in scens:
        if all( (not(greater(sc,scen)) or ("true" in prot_res[sc])) for sc in prot_res.keys()):
            res = call_check(prot, "LOGINU", scen)+call_check(prot, "LOGINT", scen)+call_check(prot, "LOGINC", scen)
            #if not(any(greater(sc,scen) and res == prot_res[sc] for sc in prot_res.keys())):
            prot_res[scen]=res
            print([prot,res,scen])
     return prot_res


def load_result_prot(prot):
     pool = Pool()
     res = pool.map(partial(load_result_prot_scenl,prot=prot),list_scenarios)
     prot_res = {}
     for prot_r in res:  
        prot_res = merge_two_dicts(prot_res, prot_r)
     return prot_res

def load_remainder_trivia(rem):
    return ["false", "false", "false"]

def load_remainder(rem):
    return call_check(rem[0], "LOGINU", rem[1])+call_check(rem[0], "LOGINT", rem[1])+call_check(rem[0], "LOGINC", rem[1])

def load_results():
    pool = Pool()
    res = pool.map(load_result_prot,prots)
    for (prot,i) in zip(prots,res):
        results[prot] = i
    pool.close()
    remainder = []
    for scen in scenarios:
        if any(scen in results[prot].keys() for prot in prots):
            for prot in prots:
                if not(scen in results[prot].keys()):
                    remainder.append((prot,scen))
    pool = Pool()
    res = pool.map(load_remainder_trivia,remainder)
    for ((prot,scen),i) in zip(remainder,res): 
        results[prot][scen] = i

def check_failures():
    for prot in prots:
        for scen in results[prot].keys():
            if 'failure' in results[prot][scen]:
                new = load_remainder([prot,scen])
                if new != results[prot][scen]:
                    print([prot,results[prot][scen],new,scen])
                    results[prot][scen] = new

   
def remove_implied_scen():
    """ Remove a scenario wich is completely implied by another one"""
    new = {}
    print(results)
    for prot in prots:
        new[prot] = {}
        for scen in results[prot].keys():
            if not(any( sc!=scen and greater(sc,scen) and all(results[prot2][scen]==results[prot2][sc] for prot2 in prots) for sc in results[prot].keys())):
                # we first check that the scenario is not a trivial consequence of another one
                new[prot][scen] = results[prot][scen]
            else:
                print("removing "+scen)
    return new

def remove_full_implied_false_scen():
    """ remove a scenario completely false and implied by others """
    new = {}
    print(results)
    for prot in prots:
        new[prot] = {}
        for scen in results[prot].keys():
            if not(" " in scen) or not( all( (results[prot][scen]== "big_false_implied") or (results[prot][scen]== "big_true_implied") for prot in prots)):
                new[prot][scen] = results[prot][scen]
            else:
                print("removing "+scen)
    return new

def remove_componly():
    """ remove scenarios not relevant to the diff """
    new = {}
    print("removing no diff scenarios")
    for prot in prots:
        new[prot] = {}
        for scen in results[prot].keys():
            if any("diff" in results[prot][scen] or any("diff" in results[prot][scen][k] for k in range(0,len(results[prot][scen]))) for prot in prots):
                new[prot][scen] = results[prot][scen]
            else:
                print("removing "+scen)
    return new

def format():
    """ We do some formating, by replacing for instance three true result for a protocol with a big true """
    new = results
    for prot in prots:
        for scen in new[prot].keys():

            for i in range(0,len(new[prot][scen])):
                if (new[prot][scen][i]=='false') and any(sc != scen and new[prot][scen][i]==new[prot][sc][i] and greater_empty(sc,scen) for sc in new[prot].keys()):
                        new[prot][scen][i] = 'false_implied'
                elif (new[prot][scen][i]=='true' and any(sc != scen and new[prot][scen][i]==new[prot][sc][i] and greater_empty(scen,sc) for sc in new[prot].keys())):
                        new[prot][scen][i] = 'true_implied'

        for scen in new[prot].keys():
            if new[prot][scen] == ['true', 'true', 'true']:
                new[prot][scen] = 'big_true'
            elif new[prot][scen] == ['true_diff', 'true_diff', 'true_diff']:
                new[prot][scen] = 'big_true_diff'
            elif new[prot][scen] == ['true_implied', 'true_implied', 'true_implied']:
                new[prot][scen] = 'big_true_implied'
            elif new[prot][scen] == ['false_implied', 'false_implied', 'false_implied']:
                new[prot][scen] = 'big_false_implied'
            elif new[prot][scen] == ['false', 'false', 'false']:
                new[prot][scen] = 'big_false'

    return new


def compare_pair_prots(pair):
    prot1=pair[0]
    prot2=pair[1]
    for scen in results[prot1].keys():
        for i in range(0,len(results[prot1][scen])):
            if not(results[prot1][scen][i] == "true" or results[prot1][scen][i] == "true_implied")  and results[prot2][scen][i] == "true":
                results[prot2][scen][i] = "true_diff"


parser = argparse.ArgumentParser()
parser.add_argument('-p','--prots', nargs='+', help='List of prots to test, all by default')
parser.add_argument('-s','--scen', nargs='+', help='List of scenarios to test, all by default')
parser.add_argument('-d','--disp', nargs='+', help='List of prots to display together')
parser.add_argument('-c','--comp', nargs='+', help='Computes the differences between two prots')
parser.add_argument('-co','--componly', help='Only displays the line with a diff', action='store_true')
parser.add_argument('-fs','--filesave', nargs='+', help='Save results into file')
parser.add_argument('-fl','--fileload', nargs='+', help='Load results from file')
parser.add_argument('-t','--timeout', type=int, help='Timeout for proverif execution')
args = parser.parse_args()
global prots
if args.prots:
    prots= args.prots
else:
    prots =(
            "g2V",
            "g2VFPR",
            "g2VD",
            "g2ST",
            "g2STFPR",
            "g2STD", 
            "g2DTFPR",
            "g2DTD",
            "g2DTDE",
            "U2F",
            "TB-U2F",
            "TB-g2DTD",
        )

if args.scen:
    list_scenarios = [args.scen]
else:    
    list_scenarios = [[''],] + cn_scenarios + acn_scenarios + device_scenarios
scenarios = [scen for elem in list_scenarios for scen in elem]
print(list_scenarios)
for prot in prots:
    check_sanity(prot)

if args.fileload:
    import numpy
    filename = args.fileload[0]
    results = numpy.load(filename+".npy").item()
else:
    results = {}

    for prot in prots:
        results[prot] = {}
    # We first compute everything with a default timeout of 3
    load_results()

    if args.timeout:
        TIMEOUT = args.timeout
    else:
        TIMEOUT = 5
    #We then rerun the failure, with a bigger timeout
    print("Checking Failures")
    check_failures()
    if args.filesave:
        import numpy
        filename = args.filesave[0]
        numpy.save(filename,results)
        
results = remove_implied_scen()

if args.comp:
    for pair in args.comp:
        pair = pair.split(" ")
        compare_pair_prots(pair)

results = format()


results = remove_full_implied_false_scen()

if args.componly:
    results = remove_componly()

print(json.dumps(results, indent=4))
if args.disp:
    gen_tex(args.disp)
gen_tex(prots)
 

